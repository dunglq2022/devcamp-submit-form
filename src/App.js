import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import FormComponent from './component/FormComponent';
import Titilecomponent from './component/TitleComponent';

function App() {
  return (
    <div className='container py-4' >
      <Titilecomponent/>
      <FormComponent/>
    </div>
  );
}

export default App;
