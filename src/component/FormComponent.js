import { Component } from "react";

class FormComponent extends Component{
    onChangeFirtName(event) {
        console.log('Thay đổi firstname')
        console.log(event.target.value)
    }
    onChangeLastName(event) {
        console.log('Thay đổi lastname')
        console.log(event.target.value)
    }
    onChangeSelectCountry(event) {
        console.log('Thay đổi country')
        console.log(event.target.value)
    }
    onChangeSubject(event) {
        console.log('Thay đổi subject')
        console.log(event.target.value)
    }
    onSubmit() {
        console.log('Form đã được submit')
    }
    render() {
        return(
            <div className="container mt-4 rounded" style={{backgroundColor: '#e9ecef'}}>
                <form>
                    <div className="form-group pt-4 pb-4 pe-2 ps-2">
                        <div className="row mt-2">
                            <label className="col-2 label-control">First Name:</label>
                            <input onChange={this.onChangeFirtName} type="text" className="col form-control" placeholder="Enter First Name" />
                        </div>
                        <div className="row mt-2">
                            <label className="col-2 label-control">Last Name:</label>
                            <input onChange={this.onChangeLastName} type="text" className="col form-control" placeholder="Enter Last Name" />
                        </div>
                        <div className="row mt-2">
                            <label className="col-2 label-control">Country:</label>
                            <select onChange={this.onChangeSelectCountry} className="form-select col">
                                <option value='0'>Chọn Country</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div className="row mt-2 mb-2">
                            <label className="col-2 label-control">Subject:</label>
                            <textarea onChange={this.onChangeSubject} className="form-control col" placeholder="Write something"></textarea>
                        </div>
                    </div>
                    <div>
                        <button onSubmit={this.onSubmit} className="btn btn-primary mb-4">send data</button>
                    </div>
                </form> 
            </div>           
        )
    }
}
export default FormComponent;